package com.dijkstra.springBootDemo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.dijkstra.springBootDemo.ServiceDemo.ServiceDemo;

@SpringBootApplication

public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext  context = SpringApplication.run(App.class, args);
        
    	ServiceDemo serivedemo =  context.getBean(ServiceDemo.class);
    	int add = serivedemo.add(5, 10);
    	System.out.println("Addition of 1 and 2 = " + add);
    	
    	
    }
}
