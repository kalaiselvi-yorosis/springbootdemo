package com.dijkstra.springBootDemo.ServiceDemo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {
	
	@Autowired
	private Personervice personservice;
	
	@RequestMapping("/service")
	public String index()
	{
		return "Service Controller";
	}
	
	@RequestMapping("/person")
	public List<Person> getallPerson()
	{
		return personservice.getAllPerson();
	}
	@RequestMapping(method = RequestMethod.POST,value = "/person")
	public void addPerson(@RequestBody Person person)
	{
		 personservice.addPerson(person);
	}

}
