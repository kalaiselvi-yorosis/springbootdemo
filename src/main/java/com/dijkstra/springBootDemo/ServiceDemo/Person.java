package com.dijkstra.springBootDemo.ServiceDemo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode

public class Person {

	private @NonNull String fname;
	private @NonNull String lname;
	private @NonNull Integer id;
	
	
	
}
