package com.dijkstra.springBootDemo.ServiceDemo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class Personervice {
	
	
	private List<Person> persons = new ArrayList<Person>();	
	
	
	public List<Person> getAllPerson()
	{
		return persons;
	}
	
	public void addPerson(Person person)
	{
		persons.add(person);
		
	}
	

}
