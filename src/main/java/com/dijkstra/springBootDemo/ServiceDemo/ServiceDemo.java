package com.dijkstra.springBootDemo.ServiceDemo;

import org.springframework.stereotype.Service;

@Service
public class ServiceDemo {

	public int add(int x, int y) {
		return x + y;
	}
	
	public int subtract(int x, int y) {
		return x - y;
	}
	
}
